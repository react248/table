import TableBody from "./TableBody";
import TableHead from "./TableHead";
import TableFoot from "./TableFoot";

function Table() {
  return (
    <table>
      <TableHead />
      <TableBody />
      <TableFoot />
    </table>
  );
}

export default Table;
